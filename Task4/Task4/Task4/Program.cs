﻿using GameEngine;
using AuxiliaryEntities;

namespace Task4
{
    class Program
    {
        static void Main(string[] args)
        {
            var render = new ConsoleRender.Render();

            Engine eng = new Engine(render, 4, 3, 15, new Size(30, 30), render.WelcomeMenu());

            eng.Run();

            //eng.LoadGame();
        }
    }
}
