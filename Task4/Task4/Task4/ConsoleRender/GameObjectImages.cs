﻿namespace Task4.ConsoleRender
{
    public static class GameObjectImages
    {
        public const char Apple = (char)3;

        public const char Bear = (char)35;

        public const char Blueberry = (char)15;

        public const char Bush = (char)5;

        public const char Cherry = (char)164;

        public const char Fox = (char)165;

        public const char Pineapple = (char)167;

        public const char Player = (char)64;

        public const char Raspberry = (char)14;

        public const char Rock = (char)127;

        public const char Wall = (char)30;

        public const char Wolf = (char)29;

        public const char Hamster = (char)2;

        public const char Hare = (char)19;

        public const char GameFieldHorizontalBorder1 = (char)30;

        public const char GameFieldHorizontalBorder2 = (char)31;

        public const char GameFieldVerticalBorder1 = (char)16;

        public const char GameFieldVerticalBorder2 = (char)17;
    }
}
