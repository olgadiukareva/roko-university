﻿using System;

namespace Task4.ConsoleRender
{
    public static class GameObjectConsoleColors
    {
        public const ConsoleColor AppleColor = ConsoleColor.Red;

        public const ConsoleColor BearColor = ConsoleColor.DarkYellow;

        public const ConsoleColor BlueBerryColor = ConsoleColor.DarkMagenta;

        public const ConsoleColor BushColor = ConsoleColor.Green;

        public const ConsoleColor CherryColor = ConsoleColor.DarkRed;

        public const ConsoleColor FoxColor = ConsoleColor.Red;

        public const ConsoleColor PineappleColor = ConsoleColor.DarkYellow;

        public const ConsoleColor PlayerColor = ConsoleColor.Cyan;

        public const ConsoleColor RaspberryColor = ConsoleColor.Magenta;

        public const ConsoleColor RockColor = ConsoleColor.DarkGray;

        public const ConsoleColor WallColor = ConsoleColor.DarkGray;

        public const ConsoleColor WolfColor = ConsoleColor.DarkGray;

        public const ConsoleColor HamsterColor = ConsoleColor.DarkRed;

        public const ConsoleColor HareColor = ConsoleColor.Green;

        public const ConsoleColor BasicLandscapeColor = ConsoleColor.DarkGreen;
    }
}
