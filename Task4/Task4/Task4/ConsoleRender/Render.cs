﻿using CommonInterfaces;
using GameEntities;
using GameEntities.ConcreteGameObjectTypes;
using System;
using System.Collections.Generic;
using AuxiliaryEntities;

namespace Task4.ConsoleRender
{
    public class Render : IRender
    {
        ConsoleColor _bufferColor;


        public Render(ConsoleColor bufferColor)
        => _bufferColor = bufferColor;

        public Render()
        {
            if (Console.WindowWidth < 80 && Console.WindowHeight < 40)
            {
                Console.WindowWidth = 80;

                Console.WindowHeight = 30;
            }

            _bufferColor = Console.ForegroundColor;
        }

        public void DrawBasicLandscape(Point point)
        {
            Console.SetCursorPosition(point.X, point.Y);
            Console.ForegroundColor = GameObjectConsoleColors.BasicLandscapeColor;
            Console.Write(BasicLandscapeImages.basicLandscapeImages[RandomGenerator.RndGen.rndGenerator.Next(0, BasicLandscapeImages.basicLandscapeImages.Length)]);
            Console.ForegroundColor = _bufferColor;
        }

        public void DrawCollectable(Collectable collectable)
        {
            Console.SetCursorPosition(collectable.Position.X, collectable.Position.Y);

            switch (collectable)
            {
                case Cherry:
                    Console.ForegroundColor = GameObjectConsoleColors.CherryColor;
                    Console.Write(GameObjectImages.Cherry);
                    break;

                case Raspberry:
                    Console.ForegroundColor = GameObjectConsoleColors.RaspberryColor;
                    Console.Write(GameObjectImages.Raspberry);
                    break;

                case BlueBerry:
                    Console.ForegroundColor = GameObjectConsoleColors.BlueBerryColor;
                    Console.Write(GameObjectImages.Blueberry);
                    break;

                case Pineapple:
                    Console.ForegroundColor = GameObjectConsoleColors.PineappleColor;
                    Console.Write(GameObjectImages.Pineapple);
                    break;

                case Apple:
                    Console.ForegroundColor = GameObjectConsoleColors.AppleColor;
                    Console.Write(GameObjectImages.Apple);
                    break;

                default:
                    break;
            }

            Console.ForegroundColor = _bufferColor;
        }

        public void DrawEnemy(Character enemy)
        {
            Console.SetCursorPosition(enemy.Position.X, enemy.Position.Y);

            switch (enemy)
            {
                case Wolf:
                    Console.ForegroundColor = GameObjectConsoleColors.WolfColor;
                    Console.Write(GameObjectImages.Wolf);
                    break;

                case Bear:
                    Console.ForegroundColor = GameObjectConsoleColors.BearColor;
                    Console.Write(GameObjectImages.Bear);
                    break;

                case Fox:
                    Console.ForegroundColor = GameObjectConsoleColors.FoxColor;
                    Console.Write(GameObjectImages.Fox);
                    break;
                case Hamster:
                    Console.ForegroundColor = GameObjectConsoleColors.HamsterColor;
                    Console.Write(GameObjectImages.Hamster);
                    break;
                case Hare:
                    Console.ForegroundColor = GameObjectConsoleColors.HareColor;
                    Console.Write(GameObjectImages.Hare);
                    break;
            }

            Console.ForegroundColor = _bufferColor;
        }

        public void DrawHorizontalFieldBorders(Size fieldSize)
        {
            DrawHorizontalBorder(fieldSize);

            Console.SetCursorPosition(0, fieldSize.Height + 1);

            DrawHorizontalBorder(fieldSize);
        }

        private void DrawHorizontalBorder(Size fieldSize)
        {
            for (int i = 0; i < fieldSize.Width / 4; i++)
            {
                Console.Write(GameObjectImages.GameFieldHorizontalBorder1.ToString() + " " + GameObjectImages.GameFieldHorizontalBorder2 + " ");
            }
        }

        public void DrawInterface(Player player, int monsterScore, int playerScore)
        {
            var fieldSize = player.GetFiledSize();

            int cursorPosition = fieldSize.Width + 4;

            ShowScores(monsterScore, playerScore, cursorPosition);

            DrawCommonPlayerInfo(player, cursorPosition);

            var bonuses = player.GetActiveBonuses();

            DrawCustomPlayerInfo(cursorPosition, bonuses);
        }

        private static void ShowScores(int monsterScore, int playerScore, int xCursorPosition)
        {
            Console.SetCursorPosition(xCursorPosition, 0);

            Console.BackgroundColor = ConsoleColor.Red;

            Console.WriteLine($"Monster score : {monsterScore}");

            Console.SetCursorPosition(xCursorPosition, 1);

            Console.BackgroundColor = ConsoleColor.Green;

            Console.WriteLine($"UserScore : {playerScore}");

            Console.BackgroundColor = ConsoleColor.Black;
        }

        private void DrawCustomPlayerInfo(int xCursorPosition, List<Collectable> bonuses)
        {
            if (bonuses.Count > 0)
            {
                Console.WriteLine("List of active bonuses :");

                for (int i = 0; i < bonuses.Count; i++)
                {
                    Console.SetCursorPosition(xCursorPosition, i + 5);

                    Console.WriteLine($"{bonuses[i].Name} {bonuses[i].EffectDuration}");
                }
            }
            else
            {
                Console.SetCursorPosition(xCursorPosition, 5);

                Console.WriteLine("List of active bonuses is empty now.");
            }
        }

        private void DrawCommonPlayerInfo(Player player, int xCursorPosition)
        {
            Console.SetCursorPosition(xCursorPosition, 2);

            Console.WriteLine($"Health : {player.Health}");

            Console.SetCursorPosition(xCursorPosition, 3);

            Console.WriteLine($"Damage : {player.BaseDamage}");

            Console.SetCursorPosition(xCursorPosition, 4);
        }

        public void DrawObstacle(Obstacle obstacle)
        {
            Console.SetCursorPosition(obstacle.Position.X, obstacle.Position.Y);

            switch (obstacle)
            {
                case Wall:
                    Console.ForegroundColor = GameObjectConsoleColors.WallColor;
                    Console.Write(GameObjectImages.Wall);
                    break;

                case Bush:
                    Console.ForegroundColor = GameObjectConsoleColors.BushColor;
                    Console.Write(GameObjectImages.Bush);
                    break;

                case Rock:
                    Console.ForegroundColor = GameObjectConsoleColors.RockColor;
                    Console.Write(GameObjectImages.Rock);
                    break;
            }

            Console.ForegroundColor = _bufferColor;
        }

        public void DrawUser(Point location)
        {
            Console.SetCursorPosition(location.X, location.Y);

            Console.ForegroundColor = GameObjectConsoleColors.PlayerColor;
            Console.Write(GameObjectImages.Player);

            Console.ForegroundColor = _bufferColor;
        }

        public void DrawVerticalFieldBorders(Size fieldSize)
        {
            DrawVerticalBorder(fieldSize, 0);

            Console.SetCursorPosition(fieldSize.Width, 0);

            DrawVerticalBorder(fieldSize, fieldSize.Width - 2);

            Console.SetCursorPosition(0, fieldSize.Height + 5);
        }

        private void DrawVerticalBorder(Size fieldSize, int x)
        {
            for (int i = 1; i < fieldSize.Height + 1; i += 2)
            {
                Console.SetCursorPosition(x, i);

                Console.Write(GameObjectImages.GameFieldVerticalBorder2);

                Console.SetCursorPosition(x, i + 1);

                Console.Write(GameObjectImages.GameFieldVerticalBorder1);
            }
        }

        public void GameOver(GameResult result)
        {
            Console.WriteLine("Game is over!");

            if (result == GameResult.Win)
                Console.WriteLine("Congratulations to a winner!!!");
            else
                Console.WriteLine("You lose, looooooseeeeer!");
        }

        public static void ShowLegend()
        {
            Console.WriteLine("Monsters:");
            Console.ForegroundColor = GameObjectConsoleColors.WolfColor;
            Console.WriteLine(GameObjectImages.Wolf + " is a wolf. It moves for a 1 map cell a turn");
            Console.ForegroundColor = GameObjectConsoleColors.BearColor;
            Console.WriteLine(GameObjectImages.Bear + " is a bear. It moves for a 2 map cell a turn");
            Console.ForegroundColor = GameObjectConsoleColors.FoxColor;
            Console.WriteLine(GameObjectImages.Fox + " is a fox. It moves for a 3 map cell a turn");
            Console.ForegroundColor = GameObjectConsoleColors.HareColor;
            Console.WriteLine(GameObjectImages.Hare + " is a hare. It moves for a 1 map cell a turn");
            Console.ForegroundColor = GameObjectConsoleColors.HamsterColor;
            Console.WriteLine(GameObjectImages.Hamster + " is a hamster. It moves for a 1 map cell a turn");
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.WriteLine("Obstacles:");
            Console.ForegroundColor = GameObjectConsoleColors.WallColor;
            Console.WriteLine(GameObjectImages.Wall + " is a wall");
            Console.ForegroundColor = GameObjectConsoleColors.RockColor;
            Console.WriteLine(GameObjectImages.Rock + " is a rock");
            Console.ForegroundColor = GameObjectConsoleColors.BushColor;
            Console.WriteLine(GameObjectImages.Bush + " is a bush");
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.WriteLine("Collectables:");
            Console.ForegroundColor = GameObjectConsoleColors.AppleColor;
            Console.WriteLine(GameObjectImages.Apple + " is an apple");
            Console.ForegroundColor = GameObjectConsoleColors.BlueBerryColor;
            Console.WriteLine(GameObjectImages.Blueberry + " is a blueberry");
            Console.ForegroundColor = GameObjectConsoleColors.CherryColor;
            Console.WriteLine(GameObjectImages.Cherry + " is a cherry");
            Console.ForegroundColor = GameObjectConsoleColors.PineappleColor;
            Console.WriteLine(GameObjectImages.Pineapple + " is a pineapple");
            Console.ForegroundColor = GameObjectConsoleColors.RaspberryColor;
            Console.WriteLine(GameObjectImages.Raspberry + " is a raspberry");
            Console.ForegroundColor = ConsoleColor.DarkGray;
        }

        public ActType ParseUserInput()
        {
            Console.SetCursorPosition(Console.WindowWidth - 1, Console.WindowHeight - 1);

            switch (Console.ReadKey().Key)
            {
                case ConsoleKey.Q:
                    return ActType.MoveLeftUp;
                case ConsoleKey.W:
                    return ActType.MoveUp;
                case ConsoleKey.E:
                    return ActType.MoveRightUp;
                case ConsoleKey.A:
                    return ActType.MoveLeft;
                case ConsoleKey.D:
                    return ActType.MoveRight;
                case ConsoleKey.S:
                    return ActType.None;
                case ConsoleKey.Z:
                    return ActType.MoveLeftDown;
                case ConsoleKey.X:
                    return ActType.MoveDown;
                case ConsoleKey.C:
                    return ActType.MoveRightDown;
                case ConsoleKey.L:
                    return ActType.LoadSave;
                case ConsoleKey.Escape:
                    return ActType.LeaveGame;
                case ConsoleKey.F2:
                    return ActType.SaveGame;
                case ConsoleKey.F3:
                    return ActType.ShowScores;
                default:
                    return ActType.None;
            }
        }

        public void ShowUserScores(Dictionary<int, List<UserScore>> scores)
        {
            Console.Clear();

            foreach (var item in scores)
            {
                Console.WriteLine("Level " + item.Key);

                if (item.Value.Count == 0)
                    Console.WriteLine("No leaders yet.");
                else
                    item.Value.ForEach(score => Console.WriteLine($"{score.UserName} - {score.Score}"));
            }

            Console.WriteLine("Press any button to resume the game.");
            Console.ReadKey();
        }

        public void ClearMap()
        => Console.Clear();

        public bool SaveGameMessage()
        {
            Console.Clear();

            do
            {
                Console.WriteLine("Save game? Y/N");

                switch (Console.ReadKey().Key)
                {
                    case ConsoleKey.Y:
                        Console.WriteLine("Game saved");
                        return true;
                    case ConsoleKey.N:
                        return false;
                    default:
                        break;
                }
            } while (true);
        }

        public int ChooseSave(List<GameSave> saves)
        {
            do
            {
                saves.ForEach(save => Console.WriteLine(save.ToString()));

                if (int.TryParse(Console.ReadLine(), out int input))
                {
                    var save = saves.Find(s => s.SaveID == input);

                    if (save is not null)
                        return save.SaveID;
                    else
                        Console.WriteLine("Selected key is not in a list.");
                }
                else
                    Console.WriteLine("Incorrect input. Please, try again.");

            } while (true);
        }

        public int ChooseLevel(Dictionary<int, string> levels)
        {
            do
            {
                ShowLevelsData(levels);

                if (int.TryParse(Console.ReadLine(), out int input))
                {
                    if (levels.ContainsKey(input))
                        return input;
                    else
                        Console.WriteLine("Selected key is not in a list.");
                }
                else
                    Console.WriteLine("Incorrect input. Please, try again.");

            } while (true);
        }

        private static void ShowLevelsData(Dictionary<int, string> levels)
        {
            Console.WriteLine("Choose level to load:");

            foreach (var item in levels)
            {
                Console.WriteLine($"{item.Key} {item.Value}");
            }
        }

        public GameSettings WelcomeMenu()
        {
            GameSettings result = new GameSettings();

            Console.WriteLine("Welcome to my game!!!");

            Console.WriteLine("Please choose game connection type:");

            result.ConnectionType = (GameConnectionType)ChooseOneEnumOption(Enum.GetValues(typeof(GameConnectionType)));
            // Hard difficulty will be added in next updates...
            result.Difficulty = GameDifficulty.Easy;

            Console.WriteLine("Please choose the type of game level:");

            result.LevelType = (GameLevelType)ChooseOneEnumOption(Enum.GetValues(typeof(GameLevelType)));

            Console.WriteLine("Before the game start, please check the next help message:");

            ShowGameControlsBasics();

            return result;
        }

        private static void ShowGameControlsBasics()
        {
            ShowLegend();

            ShowControls();

            do
            {
                Console.WriteLine("Please, enter \"IMREADY\" to start the game");
                if (Console.ReadLine() == "IMREADY")
                    break;
                else
                    Console.WriteLine("Input is incorrect.");
            } while (true);
        }

        private static void ShowControls()
        {
            Console.WriteLine("To move use next buttons:");
            Console.WriteLine("Q - move up to the left");
            Console.WriteLine("W - move up");
            Console.WriteLine("E - move up to the right");
            Console.WriteLine("A - move left");
            Console.WriteLine("S - pass the turn(you stand still, when enemies can move)");
            Console.WriteLine("D - move right");
            Console.WriteLine("Z - move down to the left");
            Console.WriteLine("X - move down");
            Console.WriteLine("C - move down to the right");
            Console.WriteLine("Other controls are:");
            Console.WriteLine("F2 - save the game");
            Console.WriteLine("L - load save");
            Console.WriteLine("ESC - leave the game");
            Console.WriteLine("F3 - show leaderbors");
        }

        private int ChooseOneEnumOption(Array enumOptions)
        {
            do
            {
                ShowEnumOptions(enumOptions);

                if (int.TryParse(Console.ReadLine(), out int input))
                {
                    if (enumOptions.Length >= input)
                        return input - 1;

                }
                else
                    Console.WriteLine("Wrong input!");
            } while (true);
        }

        private void ShowEnumOptions(Array enumOptions)
        {
            int counter = 1;

            foreach (var item in enumOptions)
            {
                Console.WriteLine($"{counter}. {item}");

                counter++;
            }
        }

        public bool ShowExitDialog()
        {
            do
            {
                Console.WriteLine("Are you sure to leave the game?(Y/N)");
                switch (Console.ReadKey().Key)
                {
                    case ConsoleKey.Y:
                        return true;
                    case ConsoleKey.N:
                        return false;
                    default:
                        Console.WriteLine("Wrong input!");
                        break;
                }
            } while (true);
        }

        public void ShowMessage(string messageText)
        => Console.WriteLine(messageText);
    }
}
