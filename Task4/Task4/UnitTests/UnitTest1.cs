using GameEntities;
using GameEntities.ConcreteGameObjectTypes;
using NUnit.Framework;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Linq;

namespace UnitTests
{

    public class InitializationTests
    {
        [Test]
        public void WrongPointInitializationTest()
        {
            Assert.Throws(typeof(System.ArgumentException), () => new AuxiliaryEntities.Point(0, 0));
        }
    }

    [TestFixture]
    public class JSONDalTests
    {
        GameEngine.Engine _localEngine;

        JsonDAL.DAO _dal;

        [OneTimeSetUp]
        public void CreateEngine()
        {
            _localEngine = new GameEngine.Engine(new Task4.ConsoleRender.Render(System.ConsoleColor.Black), 1, 1, 1, new AuxiliaryEntities.Size(20, 20), new AuxiliaryEntities.GameSettings()
            { ConnectionType = AuxiliaryEntities.GameConnectionType.Local, Difficulty = AuxiliaryEntities.GameDifficulty.Easy, LevelType = AuxiliaryEntities.GameLevelType.PreviouslyGenerated });

            _dal = new JsonDAL.DAO();
        }

        [Test]
        public void StaticConstructorTest()
        {
            Assert.That(Directory.Exists(JsonDAL.DAO._localLevelsDir));

            Assert.That(Directory.Exists(JsonDAL.DAO._localSavesDir));
        }

        [Test]
        public void EngineCreationTest()
        {
            Assert.That(_localEngine is not null);
        }

        [Test]
        public void EngineMapCreationTest()
        {
            _localEngine.CreateMap();

            Assert.That(_localEngine.GetType().GetField("_map", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(_localEngine) is not null);
        }

        [Test]
        public void EngineSaveGameExecutionTest()
        {
            _localEngine.CreateMap();

            Assert.IsTrue(_dal.SaveGame(_localEngine.GetType().GetField("_map", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(_localEngine) as List<GameObject>, "oleg"));
        }

        [Test]
        public void EngineLoadGameExecutionTest()
        {
            Assert.That(_dal.LoadGame(_dal.GetSaves()[0].SaveID) is not null);
        }

        [Test]
        public void LoadStatistics()
        {
            Assert.That(_dal.LoadStatistics() is null);
        }

        [Test]
        public void SaveGameObjectTest()
        {
            Bear b = new Bear(new GameEntities.Actors.CharacterActor(), new AuxiliaryEntities.Point(100, 100), new AuxiliaryEntities.Size(200, 200));

            var serializedData = _dal.SerializeObject(b);

            List<GameObject> gameObjects = new List<GameObject>();

            _dal.LoadGameObject(gameObjects, serializedData.type, serializedData.serializedData);

            Assert.That(b.Position == gameObjects[0].Position && b.Size.Equals(gameObjects[0].Size));
        }

        [Test]
        public void AddNewScore()
        {
            Assert.IsTrue(!_dal.AddNewScore("razdolbai", 222222, 3));
        }

        [Test]
        public void LoadLevels()
        {
            Assert.That(_dal.LoadLevel(1) is not null);
        }
    }

    public class SQLDalTests
    {
        GameEngine.Engine _netEngine;

        SQLDAL.DAO _dal;

        [OneTimeSetUp]
        public void CreateEngine()
        {
            _netEngine = new GameEngine.Engine(new Task4.ConsoleRender.Render(System.ConsoleColor.Black), 1, 1, 1, new AuxiliaryEntities.Size(20, 20), new AuxiliaryEntities.GameSettings()
            { ConnectionType = AuxiliaryEntities.GameConnectionType.Net, Difficulty = AuxiliaryEntities.GameDifficulty.Easy, LevelType = AuxiliaryEntities.GameLevelType.PreviouslyGenerated });

            _dal = new SQLDAL.DAO();
        }

        [Test]
        public void EngineCreationTest()
        {
            Assert.That(_netEngine is not null);
        }

        [Test]
        public void EngineMapCreationTest()
        {
            _netEngine.CreateMap();

            Assert.That(_netEngine.GetType().GetField("_map", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(_netEngine) is not null);
        }

        [Test]
        public void EngineSaveGameExecutionTest()
        {
            Assert.IsTrue(_dal.SaveGame(_netEngine.GetType().GetField("_map", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(_netEngine) as List<GameObject>, "oleg"));
        }

        [Test]
        public void EngineLoadGameExecutionTest()
        {
            Assert.That(_dal.LoadGame(_dal.GetSaves()[0].SaveID) is not null);
        }

        [Test]
        public void LoadStatistics()
        {
            Assert.That(_dal.LoadStatistics() is not null);
        } 

        [Test]
        public void AddNewScore()
        {
            Assert.IsTrue(_dal.AddNewScore("shalopai", 222222, 3));
        }

        [Test]
        public void LoadLevels()
        {
            var levelData = _dal.LoadLevel(_dal.GetLevels().ElementAt(0).Key);
            Assert.That(levelData is not null);
        }

        [Test]
        public void SaveCharacterTest()
        {
            Assert.That(_dal.SaveCharacter(new Bear(new GameEntities.Actors.CharacterActor(), new AuxiliaryEntities.Point(100, 100), new AuxiliaryEntities.Size(200, 200)), _dal.GetSaves()[0].SaveID)); 
        }

        [Test]
        public void SaveCollectableTest()
        {
            Assert.That(_dal.SaveCollectable(new Cherry(new GameEntities.Actors.CollectableActor(), new AuxiliaryEntities.Point(100, 100), new AuxiliaryEntities.Size(200, 200)), _dal.GetSaves()[0].SaveID));
        }

        [Test]
        public void SaveObstacleTest()
        {
            Assert.That(_dal.SaveObstacle(new Wall(new GameEntities.Actors.ObstacleActor(), new AuxiliaryEntities.Point(100, 100), new AuxiliaryEntities.Size(200, 200)), _dal.GetSaves()[0].SaveID));
        }
    }

}