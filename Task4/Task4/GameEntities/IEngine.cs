﻿using AuxiliaryEntities;
using GameEntities.ConcreteGameObjectTypes;
using System.Collections.Generic;

namespace GameEntities
{
    public interface IEngine
    {
        Size GetFieldSize();
        Player GetPlayer();
        List<GameObject> GetItemsToAdd();
    }
}
