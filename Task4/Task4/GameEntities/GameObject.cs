﻿using AuxiliaryEntities;
using GameEntities.Actors;
using System.Collections.Generic;
using Newtonsoft.Json;
using GameEntities.ConcreteGameObjectTypes;


namespace GameEntities
{
    public abstract class GameObject
    {
        protected BaseActor _actor;

        [JsonProperty]
        public Point Position { get; private set; }

        [JsonProperty]
        public Size Size { get; private set; }

        [JsonConstructor]
        public GameObject() { }

        public GameObject(Point position, BaseActor actor, Size fieldSize)
        {
            Position = position;

            Size = fieldSize;

            _actor = actor;
        }

        public GameObject(BaseActor actor, Point location)
        {
            _actor = actor;

            Position = location;

            Size = new Size();
        }

        protected void SetSize(Size newSize) => Size = newSize;

        protected void SetPosition(Point newPosition) => Position = newPosition;

        public abstract void Act(List<GameAction> surroundings);

        public void InitializeFromEngine(IEngine engine)
        {
            if (_actor is IPlayerRelatedActor)
                ((IPlayerRelatedActor)_actor).SetPlayer(engine.GetPlayer());

            if (_actor is IItemsToAddRelatedActor)
                ((IItemsToAddRelatedActor)_actor).SetMap(engine.GetItemsToAdd());

            Size = engine.GetFieldSize();
        }

        public Size GetFiledSize() => new Size(Size.Height, Size.Width);

        public abstract void AddActor();
    }
}
