﻿using AuxiliaryEntities;
using Interfaces;
using System;
using System.Collections.Generic;
using GameEntities.Actors;
using Newtonsoft.Json;

namespace GameEntities
{

    public abstract class Character : GameObject, IMovable, IHittable, IDestroyable, IDamagable
    {
        bool _isAlive;
        int _health;
        int _moveStep;
        int _baseDamage;
        List<Collectable> _activeBonuses;

        [JsonProperty]
        public bool IsAlive
        {
            get => _isAlive;
            private set
            {
                _isAlive = value;
            }
        }

        [JsonProperty]
        public int Health
        {
            get => _health;
            internal set
            {
                if (value < 0)
                    throw new ArgumentException("Health can't be below zero!");
                else
                    _health = value;
            }
        }

        [JsonProperty]

        public int MoveStep
        {
            get => _moveStep;
            internal set
            {
                if (value < 0)
                    throw new ArgumentException("Move step can't be below zero!");
                else
                    _moveStep = value;
            }
        }

        [JsonProperty]
        public int BaseDamage
        {
            get => _baseDamage;
            internal set
            {
                if (value < 0)
                    throw new ArgumentException("Base damage can't be below zero!");
                else
                    _baseDamage = value;
            }
        }

        [JsonConstructor]
        public Character() { _activeBonuses = new List<Collectable>(); }

        public Character(int health, int moveStepValue, int baseDamageValue, Size fieldBounds, BaseActor actor, Point position)
            : base(position, actor, fieldBounds)
        {
            Health = health;

            MoveStep = moveStepValue;

            BaseDamage = baseDamageValue;

            SetSize(fieldBounds);

            _activeBonuses = new List<Collectable>();

            _isAlive = true;
        }

        #region IMovable implementatiom
        public void MoveDown()
        {
            if (Position.Y + _moveStep < Size.Height)
                SetPosition(new Point(Position.X, Position.Y + _moveStep));
            else
                SetPosition(new Point(Position.X, Size.Height));
        }

        public void MoveLeft()
        {
            if (Position.X - _moveStep > 0)
                SetPosition(new Point(Position.X - _moveStep, Position.Y));
            else
                SetPosition(new Point(1, Position.Y));
        }

        public void MoveRight()
        {
            if (Position.X + _moveStep < Size.Width - 3)
                SetPosition(new Point(Position.X + _moveStep, Position.Y));
            else
                SetPosition(new Point(Size.Width - 3, Position.Y));
        }

        public void MoveUp()
        {
            if (Position.Y - _moveStep > 0)
                SetPosition(new Point(Position.X, Position.Y - _moveStep));
            else
                SetPosition(new Point(Position.X, 1));
        }

        #endregion

        #region IHittable implementation
        public int Attack()
        {
            return 1;
        }

        public int Punch()
        => (int)(_baseDamage * 1.5);

        public int Slam()
        => _baseDamage * 2;

        public int Smash()
        => _baseDamage * 3;

        public int Hit()
        => _baseDamage;

        public int Kick()
        => (int)(_baseDamage * 0.8);
        #endregion

        public void Destroy()
        => IsAlive = false;

        public void TakeDamage(int damage)
        {
            if (_health - damage > 0)
                _health -= damage;
            else
                Destroy();
        }

        public List<Collectable> GetActiveBonuses() => new List<Collectable>(_activeBonuses);

        internal void CheckBonuses()
        {
            foreach (var item in _activeBonuses)
            {
                item.ActivateBonus(this);

                item.Act();

                if (item.EffectDuration <= 0)
                    item.RemoveBonusEffect(this);
            }

            _activeBonuses.RemoveAll(bonus => bonus.EffectDuration <= 0);
        }

        public void ApplyBonus(Collectable bonus)
        {
            if (_activeBonuses is null)
                _activeBonuses = new List<Collectable>();
            else
                _activeBonuses.Add(bonus);
        }

        public override void Act(List<GameAction> surroundings)
        => _actor.ChooseAction(surroundings, this);

        public override void AddActor()
        {
            if (_actor is null)
                _actor = new CharacterActor();
        }
    }
}
