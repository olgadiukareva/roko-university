﻿using AuxiliaryEntities;
using GameEntities.Actors;
using System.Collections.Generic;

namespace GameEntities
{
    public abstract class Obstacle : GameObject
    {
        public Obstacle() { }

        public Obstacle(ObstacleActor actor, Point position) : base(actor, position) { }

        public Obstacle(Point location, Size size, ObstacleActor actor) : base(location, actor, size) { }

        public override void Act(List<GameAction> surroundingsk) { }

        public override void AddActor()
        {
            if (_actor is null)
                _actor = new ObstacleActor();
        }
    }
}
