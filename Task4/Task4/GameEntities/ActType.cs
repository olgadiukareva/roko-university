﻿namespace GameEntities
{
    public enum ActType
    {
        None = 255,
        MoveUp = 2,
        MoveDown = 7,
        MoveRight = 5,
        MoveLeft = 4,
        MoveLeftDown = 6,
        MoveLeftUp = 1,
        MoveRightDown = 8,
        MoveRightUp = 3,
        UpdateBonusDuration =9,
        SaveGame = 10,
        LoadSave = 11,
        LeaveGame = 12,
        ShowScores = 13
    }
}
