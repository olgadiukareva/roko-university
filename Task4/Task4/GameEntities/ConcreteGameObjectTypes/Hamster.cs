﻿using GameEntities.Actors;
using AuxiliaryEntities;
using System.Collections.Generic;

namespace GameEntities.ConcreteGameObjectTypes
{
    public class Hamster : Character
    {
        public Hamster()
        {
            _actor = new BursuerCharacterActor();
        }

        public Hamster(CharacterActor actor, Point position, Size fieldSize) : base(50, 1, 0, fieldSize, actor, position)
        {
            _actor = new BursuerCharacterActor();
        }

        public Hamster(int health, int baseDamage, int moveStep, Size fieldSize, CharacterActor actor, Point position) : base(health, moveStep, baseDamage, fieldSize, actor, position)
        {
            _actor = new BursuerCharacterActor();
        }
    }
}
