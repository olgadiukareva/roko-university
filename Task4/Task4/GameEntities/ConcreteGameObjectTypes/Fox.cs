﻿using GameEntities.Actors;
using AuxiliaryEntities;

namespace GameEntities.ConcreteGameObjectTypes
{
    public class Fox : Character
    {
        public Fox()
        {
            _actor = new CharacterActor();
        }

        public Fox(CharacterActor actor, Point position, Size size) : base(15, 2, 10, size, actor, position)
        {
            _actor = new CharacterActor();
        }

        public Fox(int health, int baseDamage, int moveStep, Size fieldSize, CharacterActor actor, Point position) : base(health, moveStep, baseDamage, fieldSize, actor, position)
        {
            _actor = new CharacterActor();
        }
    }
}
