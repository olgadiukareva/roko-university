﻿namespace GameEntities
{
    public enum CharacterType
    {
        Bear = 1,
        Fox = 2,
        Wolf = 3,
        Hamster = 4,
        Hare = 5,
        Player = 6
    }
}
