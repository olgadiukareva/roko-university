﻿namespace GameEntities.ConcreteGameObjectTypes
{
    public enum CollectableType
    {
        Apple = 1,
        BlueBerry = 2,
        Cherry = 3,
        Pineapple = 4,
        Raspberry = 5
    }
}
