﻿using System;
using AuxiliaryEntities;
using GameEntities.Actors;
using Newtonsoft.Json;

namespace GameEntities.ConcreteGameObjectTypes
{
    public class Pineapple : Collectable
    {

        [JsonConstructor]
        public Pineapple()
        {
            Name = "Pineapple";

            _actor = new CollectableActor();

            _effectOnUser += (player) => player.BaseDamage = player.Health + 20;

            _effectOnEnemy += (enemy) => enemy.Health = enemy.BaseDamage + 10;
        }

        public Pineapple(CollectableActor actor, Point position, Size size)
            : base(position, (player) => player.BaseDamage = player.Health + 20, (enemy) => enemy.Health = enemy.BaseDamage + 10, int.MaxValue, actor, size)
        {
            Name = "Pineapple";

            _actor = new CollectableActor();
        }

        public Pineapple(Action<Character> userEffect, Action<Character> enemyEffect, int effectDuration, CollectableActor actor, Point position, Size size)
            : base(position, userEffect, enemyEffect, effectDuration, actor, size)
        {
            Name = "Pineapple";

            _actor = new CollectableActor();
        }

        public Pineapple(Point position, int effectDuration, CharacterActor actor, Size fieldSize) : base(position, effectDuration, actor, fieldSize)
        {
            _effectOnUser += (player) => player.BaseDamage = player.Health + 20;

            _effectOnEnemy += (enemy) => enemy.Health = enemy.BaseDamage + 10;

            Name = "Pineapple";

            _actor = new CollectableActor();
        }
    }
}

