﻿using System;
using AuxiliaryEntities;
using GameEntities.Actors;
using Newtonsoft.Json;

namespace GameEntities.ConcreteGameObjectTypes
{
    public class Apple : Collectable
    {
        [JsonConstructor]
        public Apple() 
        { 
            _actor = new CollectableActor();

            _effectOnUser += (player) => player.Health = player.Health + 5;

            _effectOnEnemy += (enemy) => enemy.BaseDamage = enemy.BaseDamage + 1;

            Name = "Apple";
        }

        public Apple(CollectableActor actor, Point position, Size size)
            : base(position, (player) => player.Health = player.Health + 5, (enemy) => enemy.BaseDamage = enemy.BaseDamage + 1, int.MaxValue, actor, size)
        {
            _actor = new CollectableActor();

            Name = "Apple";
        }

        public Apple(Action<Character> userEffect, Action<Character> enemyEffect, int effectDuration, CollectableActor actor, Point position, Size size)
            : base(position, userEffect, enemyEffect, effectDuration, actor, size)
        {
            _actor = new CollectableActor();

            Name = "Apple";
        }
        public Apple(Point position, int effectDuration, CharacterActor actor, Size fieldSize) : base(position, effectDuration, actor, fieldSize)
        {
            _effectOnUser += (player) => player.Health = player.Health + 5;

            _effectOnEnemy += (enemy) => enemy.BaseDamage = enemy.BaseDamage + 1;

            Name = "Apple";

            _actor = new CollectableActor();
        }
    }
}
