﻿using GameEntities.Actors;
using AuxiliaryEntities;
using RandomGenerator;
using System.Collections.Generic;

namespace GameEntities.ConcreteGameObjectTypes
{
    public class Hare: Character
    {
        public Hare()
        {
            _actor = new FugitiveCharacterActor();
        }

        public Hare(CharacterActor actor, Point position, Size fieldSize) : base(30, 1, 0, fieldSize, actor, position)
        {
            _actor = new FugitiveCharacterActor();
        }

        public Hare(int health, int baseDamage, int moveStep, Size fieldSize, CharacterActor actor, Point position) : base(health, moveStep, baseDamage, fieldSize, actor, position)
        {
            _actor = new FugitiveCharacterActor();
        }

        public Collectable SetBonuses()
        {
            Point point = new Point { X = Position.X, Y = Position.Y };
            switch (RndGen.rndGenerator.Next(1, 4))
            {
                case 1:
                    return new BlueBerry(null, point, Size);                    
                case 2:
                    return new Apple(null, point, Size);
                case 3:
                    return new Raspberry(null, point, Size);
                default:
                    return new Pineapple(null, point, Size);
            }
        }
    }
}
