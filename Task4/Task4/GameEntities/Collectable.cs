﻿using System;
using Interfaces;
using GameEntities.Actors;
using System.Collections.Generic;
using AuxiliaryEntities;
using Newtonsoft.Json;

namespace GameEntities
{
    public abstract class Collectable : GameObject, IDestroyable
    {
        bool _isActive;

        protected Action<Character> _effectOnUser { get; set; }

        protected Action<Character> _effectOnEnemy { get; set; }

        [JsonProperty]
        public int EffectDuration { get; protected set; }

        public string Name { get; set; }

        
        public Collectable() { }

        public Collectable(Point postion, BaseActor actor, Size fieldSize) : base(postion, actor, fieldSize)
        {
            _effectOnUser += (character) => character.Health = character.Health + 1;

            _effectOnEnemy += (enemy) => enemy.BaseDamage = enemy.BaseDamage + 1;

            EffectDuration = 1;

            _isActive = false;
        }

        public Collectable(Point postion, Action<Character> userEffect, Action<Character> enemyEffect, int effectDuration, BaseActor actor, Size size)
            : base(postion, actor, size)
        {
            EffectDuration = effectDuration;

            if (userEffect != null)
                _effectOnUser += userEffect;
            else
                throw new ArgumentNullException("User effect can't be null!");

            if (enemyEffect != null)
                _effectOnEnemy += enemyEffect;
            else
                throw new ArgumentNullException("Enemy effect can't be null");

            _isActive = false;
        }

        public Collectable(Point position, int effectDuration, CharacterActor actor, Size fieldSize) : base(position, actor, fieldSize)
        {
            EffectDuration = effectDuration;
        }

        public void ActivateBonus(Character subject)
        {
            if (!_isActive)
            {
                _isActive = true;

                if (subject is ConcreteGameObjectTypes.Player)
                    _effectOnUser(subject);
                else
                    _effectOnEnemy(subject);
            }
        }

        public void Destroy()
        {
            _isActive = false;

            EffectDuration = -1;
        }

        public virtual void RemoveBonusEffect(Character subject)
        {
            if (EffectDuration > 0)
                Destroy();
        }

        public override void Act(List<GameAction> surroundings) { }

        public void Act()
        {
            (_actor as CollectableActor).Act(ActType.UpdateBonusDuration, this);
        }

        public void UpdateDuration()
        => EffectDuration--;

        public override void AddActor()
        {
            if (_actor is null)
                _actor = new CollectableActor();
        }
    }
}
