﻿using System.Collections.Generic;

namespace GameEntities.Actors
{
    public class CollectableActor : BaseActor
    {
        public override void Act(ActType actionType, GameObject subject)
        {
            if (actionType == ActType.UpdateBonusDuration)
                (subject as Collectable).UpdateDuration();
        }

        public override void ChooseAction(List<GameAction> objectSurroundings, GameObject subject)
        {
            throw new System.NotImplementedException();
        }
    }
}
