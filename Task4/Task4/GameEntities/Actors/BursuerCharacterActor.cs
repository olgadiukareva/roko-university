﻿using System;
using System.Collections.Generic;
using GameEntities.ConcreteGameObjectTypes;
using System.Linq;
using RandomGenerator;

namespace GameEntities.Actors
{
    class BursuerCharacterActor : CharacterActor, IPlayerRelatedActor
    {
        private Player _player;
        public void SetPlayer(Player player)
        {
            _player = player;
        }

        public override void ChooseAction(List<GameAction> objectSurroundings, GameObject subject)
        {
            var character = subject as Character;

            int diffX = character.Position.X - _player.Position.X;

            int diffY = character.Position.Y - _player.Position.Y;

            if (Math.Abs(diffX) <= 5 && Math.Abs(diffY) <= 5)
            {
                if (diffX < 0 && diffY < 0)
                    Act(ActType.MoveRightDown, subject);
                else if (diffX > 0 && diffY > 0)
                    Act(ActType.MoveLeftUp, subject);
                else if (diffX < 0 && diffY > 0)
                    Act(ActType.MoveRightUp, subject);
                else if (diffX > 0 && diffY < 0)
                    Act(ActType.MoveLeftDown, subject);
                else if (diffX == 0 && diffY < 0)
                    Act(ActType.MoveDown, subject);
                else if (diffX > 0 && diffY == 0)
                    Act(ActType.MoveLeft, subject);
                else if (diffX == 0 && diffY > 0)
                    Act(ActType.MoveUp, subject);
                else if (diffX < 0 && diffY == 0)
                    Act(ActType.MoveRight, subject);
            }
            else
            {
                Act((ActType)RndGen.rndGenerator.Next(1, 8), subject);
            }
        }
    }
}

