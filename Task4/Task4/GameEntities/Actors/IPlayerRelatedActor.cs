﻿using GameEntities.ConcreteGameObjectTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameEntities.Actors
{
    public interface IPlayerRelatedActor
    {
        void SetPlayer(Player player);
    }
}
