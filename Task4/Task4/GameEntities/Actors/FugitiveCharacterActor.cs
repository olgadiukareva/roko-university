﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameEntities.ConcreteGameObjectTypes;
using RandomGenerator;

namespace GameEntities.Actors
{
    public class FugitiveCharacterActor : CharacterActor, IItemsToAddRelatedActor, IPlayerRelatedActor
    {
        private Player _player;

        private List<GameObject> _itemToAdd;

        private int _frequencyOfAddingBonuses = 0;

        public void SetMap(List<GameObject> itemToAdd)
        {
            _itemToAdd = itemToAdd;
        }
        public void SetPlayer(Player player)
        {
            _player = player;
        }
        public override void ChooseAction(List<GameAction> objectSurroundings, GameObject subject)
        {
            var character = subject as Character;

            int diffX = character.Position.X - _player.Position.X;

            int diffY = character.Position.Y - _player.Position.Y;

            if (Math.Abs(diffX) <= 5 && Math.Abs(diffY) <= 5)
            {
                if (diffX < 0 && diffY < 0)
                    Act(ActType.MoveLeftUp, subject);
                else if (diffX > 0 && diffY > 0)
                    Act(ActType.MoveRightDown, subject);
                else if (diffX < 0 && diffY > 0)
                    Act(ActType.MoveLeftDown, subject);
                else if (diffX > 0 && diffY < 0)
                    Act(ActType.MoveRightUp, subject);
                else if (diffX == 0 && diffY < 0)
                    Act(ActType.MoveUp, subject);
                else if (diffX > 0 && diffY == 0)
                    Act(ActType.MoveRight, subject);
                else if (diffX == 0 && diffY > 0)
                    Act(ActType.MoveDown, subject);
                else if (diffX < 0 && diffY == 0)
                    Act(ActType.MoveLeft, subject);
            }
            else
            {
                Act((ActType)RndGen.rndGenerator.Next(1, 8), subject);
            }
            _frequencyOfAddingBonuses++;
            if(_frequencyOfAddingBonuses == 10)
            {
                Collectable bonus = ((Hare)character).SetBonuses();

                _itemToAdd.Add(bonus);
            }
        }
    }
}

