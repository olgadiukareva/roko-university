﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameEntities.Actors
{
    public interface IItemsToAddRelatedActor
    {
        void SetMap(List<GameObject> map);
    }
}
