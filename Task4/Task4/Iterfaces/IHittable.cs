﻿namespace Interfaces
{
    public interface IHittable
    {
        int Attack();
    }
}
