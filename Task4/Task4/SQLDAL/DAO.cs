﻿using System;
using System.Collections.Generic;
using DALInterface;
using GameEntities;
using System.Data;
using System.Data.SqlClient;
using GameEntities.ConcreteGameObjectTypes;
using GameEntities.Actors;
using AuxiliaryEntities;
using System.Linq;

namespace SQLDAL
{
    public class DAO : IDALInterface
    {
        public List<GameObject> LoadGame(int saveNum)
        {
            List<GameObject> result = new List<GameObject>();

            SqlConnection connection = new SqlConnection(Common.ConnectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("LoadSave", connection);

                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@SaveID", saveNum);

                var reader = command.ExecuteReader();

                result.AddRange(ReadCharacters(reader));

                result.AddRange(ReadObstacles(reader));

                result.AddRange(ReadBonuses(reader));
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                connection.Close();
            }

            return result;
        }

        private List<Character> ReadCharacters(SqlDataReader reader)
        {
            Dictionary<int, Character> characters = new Dictionary<int, Character>();

            while (reader.Read())
            {
                var characterData = ConstructCharacter(reader);
                if (characterData.id > 0)
                    characters.Add(characterData.id, characterData.character);
            }

            reader.NextResult();

            while (reader.Read())
            {
                var data = ConstructCharacterBonus(reader);
                if (data.id > 0 && characters.ContainsKey(data.id))
                    characters[data.id].ApplyBonus(data.bonus);
            }

            return characters.Values.ToList();
        }

        private (int id, Character character) ConstructCharacter(SqlDataReader reader)
        {
            switch ((CharacterType)reader["Type"])
            {
                case CharacterType.Bear:
                    return ((int)reader["ID"],
                        new Bear((int)reader["Health"], (int)reader["Damage"], (int)reader["MoveStep"],
                        new Size((int)reader["GameFieldWidth"], (int)reader["GameFieldHeight"]), new CharacterActor(),
                        new Point((int)reader["X"], (int)reader["Y"])));
                case CharacterType.Fox:
                    return ((int)reader["ID"],
                         new Fox((int)reader["Health"], (int)reader["Damage"], (int)reader["MoveStep"],
                         new Size((int)reader["GameFieldWidth"], (int)reader["GameFieldHeight"]), new CharacterActor(),
                         new Point((int)reader["X"], (int)reader["Y"])));
                case CharacterType.Wolf:
                    return ((int)reader["ID"],
                        new Wolf((int)reader["Health"], (int)reader["Damage"], (int)reader["MoveStep"],
                        new Size((int)reader["GameFieldWidth"], (int)reader["GameFieldHeight"]), new CharacterActor(),
                        new Point((int)reader["X"], (int)reader["Y"])));
                case CharacterType.Hamster:
                    return ((int)reader["ID"],
                        new Hamster((int)reader["Health"], (int)reader["Damage"], (int)reader["MoveStep"],
                        new Size((int)reader["GameFieldWidth"], (int)reader["GameFieldHeight"]), new CharacterActor(),
                        new Point((int)reader["X"], (int)reader["Y"])));
                case CharacterType.Hare:
                    return ((int)reader["ID"],
                        new Hare((int)reader["Health"], (int)reader["Damage"], (int)reader["MoveStep"],
                        new Size((int)reader["GameFieldWidth"], (int)reader["GameFieldHeight"]), new CharacterActor(),
                        new Point((int)reader["X"], (int)reader["Y"])));
                case CharacterType.Player:
                    return ((int)reader["ID"],
                        new Player((int)reader["Health"], (int)reader["Damage"], (int)reader["MoveStep"],
                        new Size((int)reader["GameFieldWidth"], (int)reader["GameFieldHeight"]), new CharacterActor(),
                        new Point((int)reader["X"], (int)reader["Y"])));
                default:
                    return (-1, null);
            }
        }

        private (int id, Collectable bonus) ConstructCharacterBonus(SqlDataReader reader)
        {
            switch ((CollectableType)reader["Type"])
            {
                case CollectableType.Apple:
                    return ((int)reader["CharacterID"], new Apple(Common.activeBonusLocation, (int)reader["EffectDuration"],
                        new CharacterActor(), new Size((int)reader["GameFieldWidth"], (int)reader["GameFieldHeight"])));
                case CollectableType.BlueBerry:
                    return ((int)reader["CharacterID"], new BlueBerry(Common.activeBonusLocation, (int)reader["EffectDuration"],
                        new CharacterActor(), new Size((int)reader["GameFieldWidth"], (int)reader["GameFieldHeight"])));
                case CollectableType.Cherry:
                    return ((int)reader["CharacterID"], new Cherry(Common.activeBonusLocation, (int)reader["EffectDuration"],
                        new CharacterActor(), new Size((int)reader["GameFieldWidth"], (int)reader["GameFieldHeight"])));
                case CollectableType.Pineapple:
                    return ((int)reader["CharacterID"], new Pineapple(Common.activeBonusLocation, (int)reader["EffectDuration"],
                        new CharacterActor(), new Size((int)reader["GameFieldWidth"], (int)reader["GameFieldHeight"])));
                case CollectableType.Raspberry:
                    return ((int)reader["CharacterID"], new Raspberry(Common.activeBonusLocation, (int)reader["EffectDuration"],
                        new CharacterActor(), new Size((int)reader["GameFieldWidth"], (int)reader["GameFieldHeight"])));
                default:
                    return (-1, null);
            }
        }

        private List<Obstacle> ReadObstacles(SqlDataReader reader)
        {
            reader.NextResult();

            List<Obstacle> result = new List<Obstacle>();

            while (reader.Read())
            {
                var obstacle = ConstructObstacle(reader);
                if (obstacle != null)
                    result.Add(obstacle);
            }

            return result;
        }

        private Obstacle ConstructObstacle(SqlDataReader reader)
        {
            switch ((ObstacleType)reader["Type"])
            {
                case ObstacleType.Bush:
                    return new Bush(new ObstacleActor(), new Point((int)reader["X"], (int)reader["Y"]),
                        new Size((int)reader["GameFieldWidth"], (int)reader["GameFieldHeight"]));
                case ObstacleType.Rock:
                    return new Rock(new ObstacleActor(), new Point((int)reader["X"], (int)reader["Y"]),
                        new Size((int)reader["GameFieldWidth"], (int)reader["GameFieldHeight"]));
                case ObstacleType.Wall:
                    return new Wall(new ObstacleActor(), new Point((int)reader["X"], (int)reader["Y"]),
                        new Size((int)reader["GameFieldWidth"], (int)reader["GameFieldHeight"]));
                default:
                    return null;
            }
        }

        private List<Collectable> ReadBonuses(SqlDataReader reader)
        {
            reader.NextResult();

            List<Collectable> result = new List<Collectable>();

            while (reader.Read())
            {
                var bonus = ConstructBonus(reader);
                if (bonus != null)
                    result.Add(bonus);
            }

            return result;
        }

        private Collectable ConstructBonus(SqlDataReader reader)
        {
            switch ((CollectableType)reader["Type"])
            {
                case CollectableType.Apple:
                    return new Apple(new Point((int)reader["X"], (int)reader["Y"]), (int)reader["EffectDuration"],
                        new CharacterActor(), new Size((int)reader["GameFieldWidth"], (int)reader["GameFieldHeight"]));
                case CollectableType.BlueBerry:
                    return new BlueBerry(new Point((int)reader["X"], (int)reader["Y"]), (int)reader["EffectDuration"],
                        new CharacterActor(), new Size((int)reader["GameFieldWidth"], (int)reader["GameFieldHeight"]));
                case CollectableType.Cherry:
                    return new Cherry(new Point((int)reader["X"], (int)reader["Y"]), (int)reader["EffectDuration"],
                        new CharacterActor(), new Size((int)reader["GameFieldWidth"], (int)reader["GameFieldHeight"]));
                case CollectableType.Pineapple:
                    return new Pineapple(new Point((int)reader["X"], (int)reader["Y"]), (int)reader["EffectDuration"],
                        new CharacterActor(), new Size((int)reader["GameFieldWidth"], (int)reader["GameFieldHeight"]));
                case CollectableType.Raspberry:
                    return new Raspberry(new Point((int)reader["X"], (int)reader["Y"]), (int)reader["EffectDuration"],
                        new CharacterActor(), new Size((int)reader["GameFieldWidth"], (int)reader["GameFieldHeight"]));
                default:
                    return null;
            }
        }

        public List<GameObject> LoadLevel(int levelID)
        {
            SqlConnection connection = new SqlConnection(Common.ConnectionString);

            string result = string.Empty;

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("LoadLevel", connection);

                command.Parameters.AddWithValue("@LevelID", levelID);

                command.CommandType = CommandType.StoredProcedure;

                var reader = command.ExecuteReader();

                while (reader.Read())
                    result = reader["LevelData"] as string;
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                connection.Close();
            }

            return new JsonDAL.DAO().DeserializeGameStamp(result.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries));
        }

        public Dictionary<int, List<UserScore>> LoadStatistics()
        {
            Dictionary<int, List<UserScore>> result = new Dictionary<int, List<UserScore>>();

            var levels = GetLevels();

            for (int i = 0; i < levels.Count; i++)
                result.Add(i + 1, LoadLevelStatistics(i));
            
            return result;
        }

        private List<UserScore> LoadLevelStatistics(int levelId)
        {
            List<UserScore> result = new List<UserScore>();

            SqlConnection connection = new SqlConnection(Common.ConnectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("GetStatistics", connection);

                command.Parameters.AddWithValue("@LevelNum", levelId);

                command.CommandType = CommandType.StoredProcedure;

                var reader = command.ExecuteReader();

                while (reader.Read())
                    result.Add(new UserScore { UserName = reader["PlayerName"] as string, Score = (int)reader["PlayerScore"] });
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                connection.Close();
            }

            return result;
        }

        public bool SaveCharacter(Character character, int saveID)
        {
            SqlConnection connection = new SqlConnection(Common.ConnectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("SaveCharacter", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters.AddWithValue("@Damage", character.BaseDamage);

                command.Parameters.AddWithValue("@Type", GetCharacterType(character));

                command.Parameters.AddWithValue("@Health", character.Health);

                command.Parameters.AddWithValue("@MoveStep", character.MoveStep);

                AddObjectPositionToACommand(character, command);

                AddCommonParametersToCommand(character, saveID, command);

                command.ExecuteNonQuery();

            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                connection.Close();
            }

            return true;
        }

        private CharacterType GetCharacterType(Character character)
        {
            switch (character)
            {
                case Bear:
                    return CharacterType.Bear;
                case Fox:
                    return CharacterType.Fox;
                case Wolf:
                    return CharacterType.Wolf;
                case Hamster:
                    return CharacterType.Hamster;
                case Hare:
                    return CharacterType.Hare;
                case Player:
                    return CharacterType.Player;
                default:
                    throw new ArgumentException("Wrong character type!");
            }
        }

        private void AddCommonParametersToCommand(GameObject gameObject, int saveID, SqlCommand command)
        {
            command.Parameters.AddWithValue("@SaveID", saveID);

            command.Parameters.AddWithValue("@FieldHeight", gameObject.GetFiledSize().Height);

            command.Parameters.AddWithValue("@FieldWidth", gameObject.GetFiledSize().Width);
        }

        private void AddObjectPositionToACommand(GameObject gameObject, SqlCommand command)
        {
            command.Parameters.AddWithValue("@X", gameObject.Position.X);

            command.Parameters.AddWithValue("@Y", gameObject.Position.Y);
        }

        public bool SaveCollectable(Collectable collectable, int saveID)
        {
            SqlConnection connection = new SqlConnection(Common.ConnectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("SaveCollectable", connection);

                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@EffectDuration", collectable.EffectDuration);

                command.Parameters.AddWithValue("@Type", GetCollectableType(collectable));

                AddObjectPositionToACommand(collectable, command);

                AddCommonParametersToCommand(collectable, saveID, command);

                command.ExecuteNonQuery();

            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                connection.Close();
            }

            return true;
        }

        private CollectableType GetCollectableType(Collectable collectable)
        {
            switch (collectable)
            {
                case Apple:
                    return CollectableType.Apple;
                case BlueBerry:
                    return CollectableType.BlueBerry;
                case Cherry:
                    return CollectableType.Cherry;
                case Pineapple:
                    return CollectableType.Pineapple;
                case Raspberry:
                    return CollectableType.Raspberry;
                default:
                    throw new ArgumentException("Wrong character type!");
            }
        }

        public bool SaveGame(List<GameObject> map, string playerName)
        {
            SqlConnection connection = new SqlConnection(Common.ConnectionString);

            int saveId;

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("SaveGame", connection);

                SqlParameter result = new SqlParameter();

                result.Direction = ParameterDirection.ReturnValue;

                result.DbType = DbType.Int32;

                command.Parameters.Add(result);

                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@PlayerName", playerName);

                command.Parameters.AddWithValue("@SaveDateTime", DateTime.Now.ToString());

                var reader = command.ExecuteNonQuery();

                saveId = (int)result.Value;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                connection.Close();
            }

            foreach (var item in map)
            {
                switch (item)
                {
                    case Collectable:
                        SaveCollectable(item as Collectable, saveId);
                        break;
                    case Obstacle:
                        SaveObstacle(item as Obstacle, saveId);
                        break;
                    case Character:
                        SaveCharacter(item as Character, saveId);
                        break;
                }
            }

            return true;
        }

        public bool SaveObstacle(Obstacle obstacle, int saveID)
        {
            SqlConnection connection = new SqlConnection(Common.ConnectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("SaveObstacle", connection);

                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@Type", GetObstacleType(obstacle));

                AddObjectPositionToACommand(obstacle, command);

                AddCommonParametersToCommand(obstacle, saveID, command);

                command.ExecuteNonQuery();

            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                connection.Close();
            }

            return true;
        }

        private ObstacleType GetObstacleType(Obstacle obstacle)
        {
            switch (obstacle)
            {
                case Wall:
                    return ObstacleType.Wall;
                case Bush:
                    return ObstacleType.Bush;
                case Rock:
                    return ObstacleType.Rock;
                default:
                    throw new ArgumentException("Wrong character type!");
            }
        }

        public List<GameSave> GetSaves()
        {
            SqlConnection connection = new SqlConnection(Common.ConnectionString);

            List<GameSave> result = new List<GameSave>();

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("GetSaves", connection);

                command.CommandType = CommandType.StoredProcedure;

                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    result.Add(new GameSave()
                    {
                        PlayerName = reader["PlayerName"] as string,
                        SaveDate = reader["SaveDate"] as string,
                        SaveID = (int)reader["ID"]
                    });
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                connection.Close();
            }

        }

        public Dictionary<int, string> GetLevels()
        {
            SqlConnection connection = new SqlConnection(Common.ConnectionString);

            Dictionary<int, string> result = new Dictionary<int, string>();

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("GetLevels", connection);

                command.CommandType = CommandType.StoredProcedure;

                var reader = command.ExecuteReader();

                while (reader.Read())
                    result.Add((int)reader["ID"], reader["LevelName"] as string);

                return result;
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                connection.Close();
            }
        }

        public bool AddNewScore(string playerName, int playerScore, int levelNum)
        {
            SqlConnection connection = new SqlConnection(Common.ConnectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("AddUserScore", connection);

                command.Parameters.AddWithValue("@PlayerName", playerName);

                command.Parameters.AddWithValue("@PlayerScore", playerScore);

                command.Parameters.AddWithValue("@LevelNum", levelNum);

                command.CommandType = CommandType.StoredProcedure;

                var reader = command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                connection.Close();
            }

            return true;
        }

        public void SaveLevel(List<GameObject> map, string levelName)
        {
            SqlConnection connection = new SqlConnection(Common.ConnectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("SaveLevel", connection);

                command.Parameters.AddWithValue("@LevelName", levelName);

                command.Parameters.AddWithValue("@JSONLevelData", new JsonDAL.DAO().SerializeMap(map));

                command.CommandType = CommandType.StoredProcedure;

                var reader = command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                return;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
