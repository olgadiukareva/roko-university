﻿namespace AuxiliaryEntities
{
    public enum GameLevelType
    {
        RandomGenerated,
        PreviouslyGenerated
    }
}
