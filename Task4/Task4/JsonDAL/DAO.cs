﻿using System;
using System.Collections.Generic;
using System.IO;
using AuxiliaryEntities;
using DALInterface;
using GameEntities;
using Newtonsoft.Json;
using GameEntities.ConcreteGameObjectTypes;
using System.Text;

namespace JsonDAL
{
    public class DAO : IDALInterface
    {
        public static readonly string _localSavesDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Saves");

        public static readonly string _localLevelsDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Levels");

        static DAO()
        {
            if (!Directory.Exists(_localSavesDir))
                Directory.CreateDirectory(_localSavesDir);

            if (!Directory.Exists(_localLevelsDir))
                Directory.CreateDirectory(_localLevelsDir);
        }

        public Dictionary<int, string> GetLevels()
        {
            Dictionary<int, string> result = new Dictionary<int, string>();

            foreach (var item in new DirectoryInfo(_localLevelsDir).GetFiles())
            {
                result.Add(result.Count, item.Name);
            }

            return result;
        }

        public List<GameSave> GetSaves()
        {
            List<GameSave> result = new List<GameSave>();

            foreach (var item in new DirectoryInfo(_localSavesDir).GetFiles())
            {
                var fileNameData = item.Name.Split();

                result.Add(new GameSave() { PlayerName = fileNameData[0], SaveDate = fileNameData[1], SaveID = result.Count });
            }

            return result;
        }

        public List<GameObject> LoadGame(int saveNum)
        => DeserializeGameStamp(File.ReadAllLines(Directory.GetFiles(_localSavesDir)[saveNum]));

        public List<GameObject> DeserializeGameStamp(string[] data)
        {
            List<GameObject> result = new List<GameObject>();

            for (int i = 0; i < data.Length; i += 2)
            {
                LoadGameObject(result, data[i], data[i + 1]);
            }

            return result;
        }

        public void LoadGameObject(List<GameObject> gameObjects, string objectType, string objectData)
        {
            if (Enum.TryParse(typeof(CharacterType), objectType, out object character))
                switch ((CharacterType)character)
                {
                    case CharacterType.Bear:
                        gameObjects.Add(JsonConvert.DeserializeObject<Bear>(objectData));
                        break;
                    case CharacterType.Fox:
                        gameObjects.Add(JsonConvert.DeserializeObject<Fox>(objectData));
                        break;
                    case CharacterType.Wolf:
                        gameObjects.Add(JsonConvert.DeserializeObject<Wolf>(objectData));
                        break;
                    case CharacterType.Hamster:
                        gameObjects.Add(JsonConvert.DeserializeObject<Hamster>(objectData));
                        break;
                    case CharacterType.Hare:
                        gameObjects.Add(JsonConvert.DeserializeObject<Hare>(objectData));
                        break;
                    case CharacterType.Player:
                        gameObjects.Add(JsonConvert.DeserializeObject<Player>(objectData));
                        break;
                }
            else if (Enum.TryParse(typeof(CollectableType), objectType, out object collectable))
                switch ((CollectableType)collectable)
                {
                    case CollectableType.Apple:
                        gameObjects.Add(JsonConvert.DeserializeObject<Apple>(objectData));
                        break;
                    case CollectableType.BlueBerry:
                        gameObjects.Add(JsonConvert.DeserializeObject<BlueBerry>(objectData));
                        break;
                    case CollectableType.Cherry:
                        gameObjects.Add(JsonConvert.DeserializeObject<Cherry>(objectData));
                        break;
                    case CollectableType.Pineapple:
                        gameObjects.Add(JsonConvert.DeserializeObject<Pineapple>(objectData));
                        break;
                    case CollectableType.Raspberry:
                        gameObjects.Add(JsonConvert.DeserializeObject<Raspberry>(objectData));
                        break;
                    default:
                        break;
                }
            else if (Enum.TryParse(typeof(ObstacleType), objectType, out object obstacle))
                switch ((ObstacleType)obstacle)
                {
                    case ObstacleType.Bush:
                        gameObjects.Add(JsonConvert.DeserializeObject<Bush>(objectData));
                        break;
                    case ObstacleType.Rock:
                        gameObjects.Add(JsonConvert.DeserializeObject<Rock>(objectData));
                        break;
                    case ObstacleType.Wall:
                        gameObjects.Add(JsonConvert.DeserializeObject<Wall>(objectData));
                        break;
                    default:
                        break;
                }
        }

        public List<GameObject> LoadLevel(int levelID)
        => DeserializeGameStamp(File.ReadAllLines(Directory.GetFiles(_localLevelsDir)[levelID]));

        public Dictionary<int, List<UserScore>> LoadStatistics()
        => null;

        public bool SaveGame(List<GameObject> map, string playerName)
        {
            var str1 = ConstructSaveName(playerName);

            using (StreamWriter writer = new StreamWriter(str1, false))
            {
                writer.Write(SerializeMap(map));
            }

            return true;
        }

        public string SerializeMap(List<GameObject> map)
        {
            StringBuilder result = new StringBuilder();

            foreach (var item in map)
            {
                var objData = SerializeObject(item);

                result.Append(objData.type + Environment.NewLine + objData.serializedData + Environment.NewLine);
            }

            return result.ToString();
        }

        public (string type, string serializedData) SerializeObject(GameObject gameObject)
        {
            switch (gameObject)
            {
                case Player character:
                    return (CharacterType.Player.ToString(), JsonConvert.SerializeObject(character));
                case Bear character:
                    return (CharacterType.Bear.ToString(), JsonConvert.SerializeObject(character));
                case Fox character:
                    return (CharacterType.Fox.ToString(), JsonConvert.SerializeObject(character));
                case Wolf character:
                    return (CharacterType.Wolf.ToString(), JsonConvert.SerializeObject(character));
                case Hamster character:
                    return (CharacterType.Hamster.ToString(), JsonConvert.SerializeObject(character));
                case Hare character:
                    return (CharacterType.Hare.ToString(), JsonConvert.SerializeObject(character));
                case Wall obstacle:
                    return (ObstacleType.Wall.ToString(), JsonConvert.SerializeObject(obstacle));
                case Rock obstacle:
                    return (ObstacleType.Rock.ToString(), JsonConvert.SerializeObject(obstacle));
                case Bush obstacle:
                    return (ObstacleType.Bush.ToString(), JsonConvert.SerializeObject(obstacle));
                case Apple collectable:
                    return (CollectableType.Apple.ToString(), JsonConvert.SerializeObject(collectable));
                case BlueBerry collectable:
                    return (CollectableType.BlueBerry.ToString(), JsonConvert.SerializeObject(collectable));
                case Cherry collectable:
                    return (CollectableType.Cherry.ToString(), JsonConvert.SerializeObject(collectable));
                case Pineapple collectable:
                    return (CollectableType.Pineapple.ToString(), JsonConvert.SerializeObject(collectable));
                case Raspberry collectable:
                    return (CollectableType.Raspberry.ToString(), JsonConvert.SerializeObject(collectable));
                default:
                    return (null, null);
            }
        }

        private string ConstructSaveName(string playerName)
        => Path.Combine(_localSavesDir, playerName + "_" + DateTime.Now.ToString().Replace("/", "").Replace('.', '_').Replace(' ', '_').Replace(':', '_') + " " + ".json");

        public bool AddNewScore(string playerName, int playeScore, int levelNum)
        => false;

        public void SaveLevel(List<GameObject> map, string levelName)
        => File.WriteAllText(Path.Combine(_localLevelsDir, levelName + ".json"), SerializeMap(map));
    }
}
