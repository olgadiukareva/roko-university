﻿using System;
using System.Collections.Generic;
using CommonInterfaces;
using GameEntities;
using AuxiliaryEntities;
using RandomGenerator;
using System.Linq;
using GameEntities.ConcreteGameObjectTypes;
using CommonInterfaces.Interactors;
using DALInterface;


namespace GameEngine
{
    public class Engine: IEngine
    {
        Player _player;
        List<GameObject> _map;
        List<GameObject> _itemsToAdd;
        Size _fieldSize;
        int _enemiesNum = 1;
        int _collectablesNum;
        int _obstaclesNum;
        int _playerScore;
        int _monsterScore;
        int _currentLevelNumber;
        IRender _render;
        IObjectIntercationProceeder _interactor;
        IDALInterface _dal;


        public Engine(IRender render, int enemiesNum, int collectablesNum, int obstaclesNum, Size fieldSize,
            GameSettings settings)
        {
            _render = render;

            _map = new List<GameObject>();

            _itemsToAdd = new List<GameObject>();

            _fieldSize = new Size(fieldSize.Height, fieldSize.Width * 2);

            _enemiesNum = enemiesNum;

            _collectablesNum = collectablesNum;

            _obstaclesNum = obstaclesNum;

            ApplySettings(settings);
        }

        private void ApplySettings(GameSettings settings)
        {
            ChooseInteractor(settings.Difficulty);

            ChooseDAL(settings.ConnectionType);

            ChooseLevelSource(settings.LevelType);
        }

        private void ChooseInteractor(GameDifficulty gameDifficulty)
        {
            switch (gameDifficulty)
            {
                case GameDifficulty.Easy:
                    _interactor = new EasyGameDifficultyInteractor(_map);
                    break;
                case GameDifficulty.Hard:
                    _interactor = new HardGameDifficultyInteractor();
                    break;
                default:
                    break;
            }
        }

        private void ChooseDAL(GameConnectionType connectionType)
        {
            switch (connectionType)
            {
                case GameConnectionType.Local:
                    _dal = new JsonDAL.DAO();

                    var levelNum = _dal.GetLevels().Count;

                    if (levelNum < 3)
                        for (int i = 0; i < 3 - levelNum; i++)
                        {
                            CreateMap();

                            _dal.SaveLevel(_map, "Level " + (i + 1).ToString());
                        }
                    break;
                case GameConnectionType.Net:
                    _dal = new SQLDAL.DAO();

                    levelNum = _dal.GetLevels().Count;

                    if (levelNum < 10)
                        for (int i = 0; i < 10 - levelNum; i++)
                        {
                            CreateMap();

                            _dal.SaveLevel(_map, "Level " + (i + 1).ToString());
                        }
                    break;
                default:
                    throw new ArgumentException("Wrong connection type.");
            }
        }

        private void ChooseLevelSource(GameLevelType levelType)
        {
            switch (levelType)
            {
                case GameLevelType.RandomGenerated:
                    _currentLevelNumber = -1;
                    break;
                case GameLevelType.PreviouslyGenerated:
                    _currentLevelNumber = 1;
                    break;
                default:
                    throw new ArgumentException("Wrong game level type.");
            }
        }

        public void Run()
        {
            Initialize();

            StartGame();
        }

        private void StartGame()
        {
            _playerScore = 0;

            _monsterScore = 0;

            while (true)
            {
                ActType input = _render.ParseUserInput();

                if ((int)input < 9)
                    _player.Act(input, GetObjectSurroundings(_player));
                else
                    ProceedNonGameActionControl(input);

                foreach (var item in _map)
                {
                    if (item is Player || item is Collectable)
                        continue;

                    item.Act(GetObjectSurroundings(item));
                }

                CheckElementsState();

                if (CheckGameOver())
                    return;

                DrawMap();
            }
        }

        private void ProceedNonGameActionControl(ActType input)
        {
            switch (input)
            {
                case ActType.None:
                    break;
                case ActType.SaveGame:
                    _render.ClearMap();
                    if (_render.SaveGameMessage())
                        SaveGame();
                    break;
                case ActType.LoadSave:
                    LoadSave();
                    break;
                case ActType.LeaveGame:
                    if (_render.ShowExitDialog())
                        GameOver(GameResult.None);
                    return;
                case ActType.ShowScores:
                    var userScores = _dal.LoadStatistics();

                    if (userScores != null)
                        _render.ShowUserScores(userScores);
                    else
                        _render.ShowMessage("There is no user score table.");
                    break;
                default:
                    break;
            }
        }

        private bool CheckGameOver()
        {
            if (_player.IsAlive && _map.Count(obj => obj is Collectable) == 0)
            {
                GameOver(GameResult.Win);

                return true;
            }

            if (!_player.IsAlive)
            {
                GameOver(GameResult.Lose);

                return true;
            }

            return false;
        }

        private void GameOver(GameResult result)
        {
            if (result == GameResult.None)
                Environment.Exit(-1);

            if (_currentLevelNumber == 10)
            {
                _render.ShowMessage("This is all game content for now. Glory to the greatest winner!");

                Environment.Exit(-1);
            }

            if (_currentLevelNumber > 0)
            {
                _dal.AddNewScore(Environment.UserName, _playerScore, _currentLevelNumber);

                _currentLevelNumber++;

                _playerScore = 0;

                _monsterScore = 0;

                Run();
            }

            _render.ClearMap();

            _render.GameOver(result);
        }

        private List<GameAction> GetObjectSurroundings(GameObject expectingItem)
        {
            List<GameAction> result = new List<GameAction>();

            var surroundingData = _map
                .FindAll(obj => IsObjectASurrounding(expectingItem, obj))
                .OrderBy(selector => selector.Position.Y)
                .ToList();

            for (int i = 0; i < surroundingData.Count(); i++)
            {
                result.Add(new GameAction(MatchActTypeWithObject(surroundingData[i], expectingItem), surroundingData[i]));
            }

            return result;
        }

        private void CheckElementsState()
        {
            foreach (var item in GetMapCell())
            {
                var objectsToDeletion = _interactor.ProceedGroup(item.ToList(), out int monsterScore, out int playerScore);

                _monsterScore += monsterScore;

                _playerScore += playerScore;

                _map.RemoveAll(obj => objectsToDeletion.Contains(obj));
            }

            _map.AddRange(_itemsToAdd);

            _itemsToAdd.Clear();
        }

        private IEnumerable<IEnumerable<GameObject>> GetMapCell()
        {
            List<GameObject> sourceObjects = new List<GameObject>(_map);

            while (sourceObjects.Count > 0)
            {
                var foundItems = sourceObjects.FindAll(obj => obj.Position == sourceObjects[0].Position);

                sourceObjects.RemoveAll(obj => foundItems.Contains(obj));

                yield return foundItems;
            }
        }

        private ActType MatchActTypeWithObject(GameObject object1, GameObject object2)
        {
            switch (GetObjectsDistanceInOY(object1, object2))
            {
                case -1:
                    switch (object1.Position.X - object2.Position.X)
                    {
                        case -1:
                            return ActType.MoveLeftUp;
                        case 0:
                            return ActType.MoveUp;
                        case 1:
                            return ActType.MoveRightUp;
                    }
                    break;
                case 0:
                    switch (object1.Position.X - object2.Position.X)
                    {
                        case -1:
                            return ActType.MoveLeft;
                        case 1:
                            return ActType.MoveRight;
                    }
                    break;
                case 1:
                    switch (object1.Position.X - object2.Position.X)
                    {
                        case -1:
                            return ActType.MoveLeftDown;
                        case 0:
                            return ActType.MoveDown;
                        case 1:
                            return ActType.MoveRightDown;
                    }
                    break;
            }

            return ActType.None;
        }

        private int GetObjectsDistanceInOY(GameObject object1, GameObject object2)
        => object1.Position.Y - object2.Position.Y;

        private bool IsObjectASurrounding(GameObject object1, GameObject object2)
        {
            var c1 = Math.Abs(object1.Position.X - object2.Position.X);

            var c2 = Math.Abs(object1.Position.Y - object2.Position.Y);

            return (c1 >= 0 && c1 < 2) && (c2 >= 0 && c2 < 2) && !(c1 == 0 && c2 == 0);
        }

        private void Initialize()
        {
            if (_currentLevelNumber < 0)
                CreateMap();
            else
                LoadLevel();

            DrawMap();
        }

        public void CreateMap()
        {
            _map.Clear();

            _currentLevelNumber = -1;

            SpawnPlayer();

            SpawnEnemies();

            SpawnCollectables();

            SpawnObstacles();

            _map.ForEach(obj => obj.InitializeFromEngine(this));
        }

        private void DrawMap()
        {
            _render.ClearMap();

            GenerateLandscape();

            _render.DrawInterface(_player, _monsterScore, _playerScore);

            foreach (var item in _map)
            {
                switch (item)
                {
                    case Player:
                        continue;
                    case Character character:
                        _render.DrawEnemy(character);
                        break;
                    case Obstacle obstacle:
                        _render.DrawObstacle(obstacle);
                        break;
                    case Collectable collectable:
                        _render.DrawCollectable(collectable);
                        break;
                }
            }

            _render.DrawUser(_player.Position);
        }

        public void LoadLevel()
        => SetUpNewMap(_dal.LoadLevel(_dal.GetLevels().ElementAt(_currentLevelNumber - 1).Key));

        private void SpawnCollectables()
        {
            var actor = new GameEntities.Actors.CollectableActor();

            for (int i = 0; i < _collectablesNum; i++)
            {
                _map.Add(new Raspberry(actor, GeneratePosition(), _fieldSize));

                _map.Add(new BlueBerry(actor, GeneratePosition(), _fieldSize));

                _map.Add(new Pineapple(actor, GeneratePosition(), _fieldSize));

                _map.Add(new Apple(actor, GeneratePosition(), _fieldSize));

                _map.Add(new Cherry(actor, GeneratePosition(), _fieldSize));
            }
        }

        private void SpawnObstacles()
        {
            var actor = new GameEntities.Actors.ObstacleActor();

            for (int i = 0; i < _obstaclesNum; i++)
            {
                _map.Add(new Rock(actor, GeneratePosition(), _fieldSize));

                _map.Add(new Wall(actor, GeneratePosition(), _fieldSize));

                _map.Add(new Bush(actor, GeneratePosition(), _fieldSize));
            }
        }

        private Point GeneratePosition()
        {
            do
            {
                int x = RndGen.rndGenerator.Next(1, _fieldSize.Width - 2);
                int y = RndGen.rndGenerator.Next(1, _fieldSize.Height);

                if (!_map.Exists((obj) => obj.Position.X == x && obj.Position.Y == y))
                    return new Point(x, y);

            } while (true);
        }

        private void GenerateLandscape()
        {
            _render.DrawHorizontalFieldBorders(_fieldSize);

            _render.DrawVerticalFieldBorders(_fieldSize);

        }

        private void SpawnEnemies()
        {
            var actor = new GameEntities.Actors.CharacterActor();

            for (int i = 0; i < _enemiesNum; i++)
            {
                _map.Add(new Wolf(actor, GeneratePosition(), _fieldSize));

                _map.Add(new Bear(actor, GeneratePosition(), _fieldSize));

                _map.Add(new Fox(actor, GeneratePosition(), _fieldSize));

                _map.Add(new Hamster(actor, GeneratePosition(), _fieldSize));

                _map.Add(new Hare(actor, GeneratePosition(), _fieldSize));
            }
        }

        private void SpawnPlayer()
        {
            _player = new Player(new GameEntities.Actors.CharacterActor(), GeneratePosition(), _fieldSize);

            _map.Add(_player);
        }

        private bool SaveGame()
        =>_dal.SaveGame(_map, Environment.UserName);
        
        public void LoadSave()
        {
            _render.ClearMap();

            var levelData = _dal.LoadGame(_render.ChooseSave(_dal.GetSaves()));

            SetUpNewMap(levelData);
        }

        private void SetUpNewMap(List<GameObject> levelData)
        {
            if (levelData is null)
                throw new NullReferenceException();

            _map = levelData;

            _player = _map.Find(obj => obj is Player) as Player;

            _map.ForEach(obj => obj.InitializeFromEngine(this));
        }
        public Size GetFieldSize()
        {
            return _fieldSize;
        }
        public Player GetPlayer()
        {
            return _player;
        }
        public List<GameObject> GetItemsToAdd()
        {
            return _itemsToAdd;
        }
    }
}
