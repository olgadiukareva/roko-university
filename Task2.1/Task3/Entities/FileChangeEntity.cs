﻿namespace Entities
{
    public class FileChangeEntity
    {
        public string FirstFileName { get; init; }

        public string SecondFileName { get; init; }        
    }
}
