﻿namespace Entities
{
    [System.Flags]
    public enum TextChangeType
    {
        None,
        Deleted,
        Appended
    }
}
