﻿namespace Entities
{
    public enum FileType
    {
        None, 
        Binary,
        Text
    }
}
