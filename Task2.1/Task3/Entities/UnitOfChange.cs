﻿using System.Collections.Generic;

namespace Entities
{
    public class UnitOfChange
    {
        public IEnumerable<FileChangeEntity> Changes { get; set; }

        public DifferenceType ChangeType { get; set; }
    }
}
