﻿using System;
using System.Collections.Generic;
using BL;
using Entities;
using System.Linq;

namespace Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 2)
                ShowHelp();
            else
            {
                ShowData(new LogicController().SearchForDifference(args[0], args[1]));
            }
        }

        static void ShowHelp()
        {
            Console.WriteLine("Hello! You just tried to use my version of git diff!");
            Console.WriteLine("To use the app, you should input names(2) of directories to compare as command line arguments");
            Console.WriteLine("Indication may be:");

            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("Yellow - for unique files");
            Console.WriteLine();

            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine("Magenta - for binary files");
            Console.WriteLine();

            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Green - for added text, " + Environment.NewLine);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("red - for removed text, " + Environment.NewLine);
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("and gray - for common text, without changes in text files");
            Console.WriteLine();
        }

        static void ShowData(IEnumerable<UnitOfChange> data)
        {
            foreach (var item in data)
            {
                switch (item.ChangeType)
                {
                    case DifferenceType.UniqueFile:
                        ShowUniqueFileDifference(item);
                        break;
                    case DifferenceType.BinaryFileDifference:
                        ShowBinaryFileDifference(item);
                        break;
                    case DifferenceType.TextFileContentDifference:
                        ShowTextFileDifference(item);
                        break;
                    case DifferenceType.None:
                    default:
                        break;
                }
            }
        }

        private static void ShowTextFileDifference(UnitOfChange item)
        {
            Console.WriteLine("Text file difference:");
            Console.WriteLine($"{item.Changes.ElementAt(0).FirstFileName + Environment.NewLine}" +
                $"VS {item.Changes.ElementAt(0).SecondFileName + Environment.NewLine}");

            Console.WriteLine(new string('-', Console.WindowWidth));

            foreach (var dataString in item.Changes.Cast<TextFileChange>())
            {
                switch (dataString.TextChangeType)
                {
                    case TextChangeType.None:
                        Console.WriteLine(dataString.Content);
                        break;

                    case TextChangeType.Appended:
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine(dataString.Content);
                        Console.ForegroundColor = ConsoleColor.Gray;
                        break;

                    case TextChangeType.Deleted:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine(dataString.Content);
                        Console.ForegroundColor = ConsoleColor.Gray;
                        break;
                }
            }

            Console.WriteLine(new string('-', Console.WindowWidth));
            Console.WriteLine();
        }

        private static void ShowBinaryFileDifference(UnitOfChange item)
        {
            Console.WriteLine("Binary file dif:");
            Console.WriteLine($"{item.Changes.ElementAt(0).FirstFileName + Environment.NewLine}" +
                $"VS {item.Changes.ElementAt(0).SecondFileName + Environment.NewLine}");

            Console.WriteLine(new string('-', Console.WindowWidth));

            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine(item.Changes.ElementAt(0).FirstFileName);
            Console.ForegroundColor = ConsoleColor.Gray;

            Console.WriteLine(new string('-', Console.WindowWidth));
            Console.WriteLine();
        }

        private static void ShowUniqueFileDifference(UnitOfChange item)
        {
            Console.WriteLine("Unique file:");
            Console.WriteLine(new string('-', Console.WindowWidth));

            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine(item.Changes.ElementAt(0).FirstFileName);
            Console.ForegroundColor = ConsoleColor.Gray;

            Console.WriteLine(new string('-', Console.WindowWidth));
            Console.WriteLine();
        }
    }
}
