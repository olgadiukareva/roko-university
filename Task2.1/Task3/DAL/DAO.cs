﻿using Entities;
using System;
using System.Collections.Generic;
using System.IO;

namespace DAL
{
    public class DAO
    {
        public static List<FileEntity> GetDirectoryContent(string pathToDirectory)
        {
            List<FileEntity> files = new List<FileEntity>();

            foreach (var item in Directory.GetFiles(pathToDirectory))
            {
                files.Add(new FileEntity() { Content = File.ReadAllText(item), FileName = item});
            }

            return files;
        }

        public static DateTime GetFileLastModifyTime(string fileName)
        => File.GetLastAccessTime(fileName);
    }
}
