﻿namespace ConsoleArgumentsValidator
{
    public sealed class CommandSignatures
    {
        public const string UseRegex = "-r";

        public const string Help1 = "-h";

        public const string Help2 = "-help";

        public const string UseLogger = "-l";
    }
}
