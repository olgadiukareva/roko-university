﻿
namespace Entities
{
    public class MatchInfo
    {
        public int StringNum { get; }

        public int CursorPosition { get; }

        public string StringText { get; }


        public MatchInfo(int stringNum, int cursorPosition, string text)
        {
            StringNum = stringNum;

            CursorPosition = cursorPosition;

            StringText = text;
        }

        public override string ToString()
        => $"String: {StringNum}, Position: {CursorPosition}, Result: {StringText}";
    }
}
