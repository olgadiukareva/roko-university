﻿using System;
using System.Diagnostics;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 2)
                ShowHelp();
            else
            {
                Stopwatch stopwatch = new Stopwatch();

                stopwatch.Start();

                LogicController.ExecuteNotOptimalAlgorithm(args[0], args[1]);

                stopwatch.Stop();

                Console.WriteLine($"Not optimal method, needed: {stopwatch.ElapsedMilliseconds} ms");

                stopwatch.Reset();

                stopwatch.Start();

                LogicController.ExecuteOptimalAlgorithm(args[0], args[1]);

                stopwatch.Stop();

                Console.WriteLine($"Optimal method, needed: {stopwatch.ElapsedMilliseconds} ms");
            }
        }

        static void ShowHelp()
        {
            Console.WriteLine("To use my app, you should input dir_name files_extentions");
            Console.WriteLine("Example: myFolder *.txt - prog will process each file in inputed directory.");
        }
    }
}
